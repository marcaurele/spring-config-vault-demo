package pki;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.vault.config.VaultSecretBackendDescriptor;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotEmpty;
import java.util.List;
import java.util.Objects;

/**
 * Configuration properties for Vault using the PKI integration.
 *
 * @author Mark Paluch
 */
@ConfigurationProperties("pki")
@Validated
public class VaultPkiProperties implements VaultSecretBackendDescriptor {

    /**
     * Enable pki backend usage.
     */
    private boolean enabled = true;

    /**
     * Role name for credentials.
     */
    @NotEmpty
    private String role;

    /**
     * pki backend path.
     */
    @NotEmpty
    private String backend = "pki";

    /**
     * The CN of the certificate. Should match the host name.
     */
    @NotEmpty
    private String commonName;

    /**
     * Alternate CN names for additional host names.
     */
    private List<String> altNames;

    /**
     * Prevent certificate re-creation by storing the Valid certificate inside Vault.
     */
    private boolean reuseValidCertificate = true;

    /**
     * Startup/Locking timeout. Used to synchronize startup and to prevent multiple SSL
     * certificate requests.
     */
    private int startupLockTimeout = STARTUP_LOCK_TIMEOUT_DEFAULT;

    /**
     * Default Startup/Locking timeout value.
     */
    public final static int STARTUP_LOCK_TIMEOUT_DEFAULT = 10000;

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String getBackend() {
        return backend;
    }

    public void setBackend(String backend) {
        this.backend = backend;
    }

    public String getCommonName() {
        return commonName;
    }

    public void setCommonName(String commonName) {
        this.commonName = commonName;
    }

    public List<String> getAltNames() {
        return altNames;
    }

    public void setAltNames(List<String> altNames) {
        this.altNames = altNames;
    }

    public boolean isReuseValidCertificate() {
        return reuseValidCertificate;
    }

    public void setReuseValidCertificate(boolean reuseValidCertificate) {
        this.reuseValidCertificate = reuseValidCertificate;
    }

    public int getStartupLockTimeout() {
        return startupLockTimeout;
    }

    public void setStartupLockTimeout(int startupLockTimeout) {
        this.startupLockTimeout = startupLockTimeout;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VaultPkiProperties that = (VaultPkiProperties) o;
        return enabled == that.enabled &&
                reuseValidCertificate == that.reuseValidCertificate &&
                startupLockTimeout == that.startupLockTimeout &&
                role.equals(that.role) &&
                backend.equals(that.backend) &&
                commonName.equals(that.commonName) &&
                Objects.equals(altNames, that.altNames);
    }

    @Override
    public int hashCode() {
        return Objects.hash(enabled, role, backend, commonName, altNames, reuseValidCertificate, startupLockTimeout);
    }

    @Override
    public String toString() {
        return "VaultPkiProperties{" +
                "enabled=" + enabled +
                ", role='" + role + '\'' +
                ", backend='" + backend + '\'' +
                ", commonName='" + commonName + '\'' +
                ", altNames=" + altNames +
                ", reuseValidCertificate=" + reuseValidCertificate +
                ", startupLockTimeout=" + startupLockTimeout +
                '}';
    }
}
