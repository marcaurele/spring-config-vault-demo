package hello;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.vault.authentication.SessionManager;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class ConfigClientApplication {

    private final static Log LOGGER = LogFactory.getLog(ConfigClientApplication.class);

    @Autowired
    private SessionManager sessionManager;

    public static void main(String[] args) {
        SpringApplication.run(ConfigClientApplication.class, args);
    }

    // This bean loads the git.properties file and expose the values to the application
    @Bean
    public static PropertySourcesPlaceholderConfigurer placeholderConfigurer() {
        PropertySourcesPlaceholderConfigurer propsConfig
                = new PropertySourcesPlaceholderConfigurer();
        propsConfig.setLocation(new ClassPathResource("git.properties"));
        propsConfig.setIgnoreResourceNotFound(true);
        propsConfig.setIgnoreUnresolvablePlaceholders(true);
        return propsConfig;
    }

    @PostConstruct
    public void initIt() {
        LOGGER.info(String.format("Got Vault Token: %s", sessionManager.getSessionToken().getToken()));
    }
}

@RefreshScope
@RestController
class MessageRestController {

    @Value("${message:Hello from Java class}")
    private String message;

    @Value("${secret:Not secret}")
    private String secret;

    @Value("${global.variable:must set a default}")
    private String globalConf;

    @Value("${git.branch:none}")
    private String gitBranch;

    @Autowired
    private Environment env;

    @RequestMapping("/message")
    @ResponseBody
    MessageOutput getMessage() {
        return new MessageOutput(env.getActiveProfiles(), this.message);
    }

    @RequestMapping("/secret")
    @ResponseBody
    MessageOutput getSecret() {
        return new MessageOutput(env.getActiveProfiles(), this.secret);
    }

    @RequestMapping("/global")
    @ResponseBody
    MessageOutput getGlobal() {
        return new MessageOutput(env.getActiveProfiles(), this.globalConf);
    }

    @RequestMapping("/git")
    @ResponseBody
    MessageOutput getGitBranch() {
        return new MessageOutput(env.getActiveProfiles(), this.gitBranch);
    }
}

class MessageOutput {
    private String[] profiles;
    private String messsage;

    public MessageOutput(String[] profiles, String messsage) {
        this.profiles = profiles;
        this.messsage = messsage;
    }

    public String[] getProfiles() {
        return profiles;
    }

    public String getMesssage() {
        return messsage;
    }
}