# Demo for the integration of HashiCorp Vault into a Java application (Spring-boot)

5 steps are tagged to show the iteration process into embedding Vault into the application.

## Step 1

This is the inception where we have only a spring-boot Java application returning a few properties on some given endpoints.

```bash
git checkout step-1
mvn clean package
cd docker
docker-compose up --build
```

Then you can check the server endpoints:

```bash
curl -s http://localhost:8080/message | jq
curl -s http://localhost:8080/secret | jq
curl -s http://localhost:8080/git | jq
```

## Step 2

```bash
git checkout step-2
mvn clean package
cd docker
docker-compose up --build
```

Then you can check the message coming from the YAML file (the secret hasn't changed):

```bash
curl -s http://localhost:8080/message | jq
curl -s http://localhost:8080/secret | jq
```

## Step 3

We're adding the spring cloud vault plugin to interact with Vault and retrieve a secret. This requires to add Vault in the `docker-compose.yml` file and populate it with a secret value at the expected location for the application:

```bash
git checkout step-3
mvn clean package
cd docker
docker-compose up --build
```

The `message` hasn't changed but the secret value is now coming from Vault:

```bash
curl -s http://localhost:8080/message | jq
curl -s http://localhost:8080/secret | jq
```

## Step 4

We're adding the TLS configuration into the application to expose the service on port 8443 and retrieve the certificat from Vault during application startup. A script `create_certificates.sh` is provided to generate a root CA and an intermediate signing CA, the latest being used to sign the certificates created by Vault. The *vault-initializer* side-car is extended to add the CA loading into Vault.

```bash
git checkout step-4
mvn clean package
cd docker/vault
./create_certificates.sh
cd ..
docker-compose up --build
```

The output will stay the same as in step-3, but now we're using the secure port 8443 (we must provide `--insecure` to `curl` in order to by pass the check on our self-signed certificate):

```bash
curl --insecure -s https://localhost:8443/message | jq
curl --insecure -s https://localhost:8443/secret | jq
```

## Step 5

An optional step where we add the spring cloud config server to retrieve a value for the key `message` from such a server with its repository hosted at https://gitlab.com/marcaurele/spring-config-vault-a-bootiful-client. Vault and Config server works on the same principal with a remote API providing a value for configuration keys.

```bash
git checkout step-5
mvn clean package
cd docker
docker-compose up --build
```
The `secret` won't change, but the value for `message` reflect the value in the git repository.

```bash
curl --insecure -s https://localhost:8443/message | jq
curl --insecure -s https://localhost:8443/secret | jq
```

## Step 6

Continuing on the spring cloud config integration, we active *profiles* to be able to differentiate variable if they should be different by environment.

```bash
git checkout step-6
mvn clean package
cd docker
docker-compose up --build
```

The `message` endpoint will return a customized value for the profile `aa` which you could map to an environment named AA. Even on the Vault side we can differentiate this environment with a different path. Check out the new values returned with those calls:

```bash
curl --insecure -s https://localhost:8443/message | jq
curl --insecure -s https://localhost:8443/secret | jq
```