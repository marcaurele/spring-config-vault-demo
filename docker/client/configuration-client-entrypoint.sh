#!/bin/sh
set -e

COUNT=1
while ! nc -z config-server 8888 ; do
    # Wait max 15 seconds
    if [ $COUNT -eq 15 ]; then
      exit 1
    fi
    echo "Waiting for upcoming configuration server $COUNT/14"
    COUNT=$(( $COUNT + 1 ))
    sleep 1
done

exec "$@"