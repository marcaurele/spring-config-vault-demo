#!/bin/sh
set -e

# Sidecar action to automatically add some entries into the vault

COUNT=1
while ! nc -z vault 8200 ; do
    # Wait max 15 seconds
    if [ $COUNT -eq 15 ]; then
      exit 1
    fi
    echo "Waiting for vault server $COUNT/14"
    COUNT=$(( $COUNT + 1 ))
    sleep 1
done

VAULT_BIN=vault
APP_NAME=a-bootiful-client

export VAULT_ADDR=http://vault:8200
export VAULT_TOKEN=devtoken

echo "###########################################################################"
echo "# Restoring non-versioned K/V backend at secret/                          #"
echo "###########################################################################"

${VAULT_BIN} secrets disable secret
${VAULT_BIN} secrets enable -path secret -version 1 kv

echo "###########################################################################"
echo "# Mounting versioned K/V backend at versioned/                            #"
echo "###########################################################################"

${VAULT_BIN} secrets enable -path versioned -version 2 kv

echo "###########################################################################"
echo "# Setup app sample data                                                   #"
echo "###########################################################################"

echo "vault kv put versioned/${APP_NAME} 'secret=application root secret'"
${VAULT_BIN} kv put versioned/${APP_NAME} 'secret=application root secret stored in Vault'
echo "vault kv put versioned/${APP_NAME}/ad 'secret=application ad-profile secret'"
${VAULT_BIN} kv put versioned/${APP_NAME}/ad 'secret=application ad-profile secret stored in Vault'
echo "vault kv put versioned/${APP_NAME}/aa 'secret=application aa-profile secret'"
${VAULT_BIN} kv put versioned/${APP_NAME}/aa 'secret=application aa-profile secret stored in Vault'

echo "###########################################################################"
echo "# Setup static PKI example                                                #"
echo "###########################################################################"

echo "vault secrets enable pki"
${VAULT_BIN} secrets enable pki

echo "write pki/config/ca pem_bundle=-"
cat work/ca/certs/intermediate.cert.pem work/ca/private/intermediate.decrypted.key.pem | ${VAULT_BIN} write pki/config/ca pem_bundle=-

echo "vault write pki/roles/localhost-tls-demo allowed_domains=localhost,example.com allow_localhost=true max_ttl=72h"
${VAULT_BIN} write pki/roles/localhost-tls-demo allowed_domains=localhost,example.com allow_localhost=true max_ttl=72h

# exec "$@"